Her er link til min MiniX1: https://salomon7695206.gitlab.io/au-programmering/MiniX1/index.html

I denne uges MiniX har jeg lavet en sketch af et kamera. Det er ret simpelt i udseendet. Jeg fik lavet nogle rektangler og nogle cirkler, så det nogenlunde ligner en kamera. Derudover fik jeg farvet kameraet.

Jeg har brugt 4 koder i min sketch. fill(x), rect(x,x,x,x), ellipse(x,x,x,x), quad(x,x,x,x,x,x,x,x). I starten så brugte jeg quad fordi det et trapez-look, men jegsyntes detså lidt mærkeligt ud, og jeg har beholdt det og tilpasset det som en rektangel, fordi jeg var for doven til, at erstatte det med en rect koden. Disse 4 koder tillod mig, at lave en sketch af kamera. 

Min første kodning har været lidt frustrerende, da jeg ikke har noget erfaringer med kodning. Det sværeste har nok været, at skulle tænke over, hvilke punkter tilhører, hvilke vinkler, og finde ud af tallene for, at kunne placere dem rigtigt. Såvel som det har været frustrerende er jeg også meget lettet over, at jeg i sidste ende kunne komme op med noget, der   så udmærket ud.  Jeg tror ikke helt, jeg har fundet helt ud af, hvordan jeg skal læse eksempler på koden, jeg har bare prøvet, at kunne afkode, hvilke punkter passer bedst med beskrivelsen af eksemplet og prøvet frem. Nogen gange fungerede det, andre gange stak det helt af!

Jeg synes, at programmet er godt, det er forholdsvist nemt, at finde ud af, når, man   er kommet i gang. Omkring programmering for mit vedkommende, hvis der er instruktioner, så kan, man   finde ud af det, men nogen gange kan referencen være svær, at forstå, men hvis man prøver sig frem kan, man i nogen tilfælde forstå referencen bedre.
